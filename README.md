# Icecast & ezstream docker-compose

### What is this?
This repo contains a docker-compose to build and run Icecast and ezstream
from scratch.


### How to build
Just clone this repo and run 

* `docker-compose build`
* `docker-compose up`

Go to http://localhost:8000 to get to the dashboard. To log into the admin panel
use username `admin` with password `hackme`. 

### How to run my own music files?
Put all your media into the `ezstream/media` folder, and include a playlist file
called `playlist.m3u`. This playlist will be played automatically when launching ezstream.

Please note that the playlist must contain absolute paths, and that the media folder 
will be mounted under `/media` in the container.  